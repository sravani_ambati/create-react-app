import React from "react";
import IndexNavbar from "components/Navbars/IndexNavbar.js";
import FooterTwo from "components/CustomFooterTwo";
import OurWorkPageContent from "./OurworkPageContent";

class OurWorkMainContent extends React.Component {
    render () {
        return (
            <>
                <IndexNavbar />
                <div 
                    className="page-header section-dark"
                    style={{
                        backgroundImage:
                        "url(" + require("assets/img/customImages/develop.jpg") + ")"
                    }}
                >
                </div>
                <OurWorkPageContent />
                <FooterTwo />
            </>
        )
    }
}

export default OurWorkMainContent;