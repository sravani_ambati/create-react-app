import React from "react";
import "./waves.scss";

class Waves extends React.Component {
    render() {
        return (
            <>
                <div className="waves-page-header">
                    <div className="ocean">
                        <div className="wave"></div>
                        <div className="wave"></div>
                    </div>
                </div>
            </>
        )
    }
}

export default Waves;