import React from "react";
import {Row, Col, Container} from "reactstrap";
import ContactFormContainer from "./ContactForm";
import "./Contact.scss";

class ContactPageContent extends React.Component {
    render() {
        return (
            <>
                <div className="contact-main">
                    <div className="heading-content">
                        <span style={{color : "#00256f"}}><b>Have Questions ? Let's</b></span>
                        <span style={{color : "#ca052e", fontWeight : "501"}}> TALK</span>
                    </div>
                    <div className="container">
                        <Row>
                            <Col lg={6} md={12} sm={12} xs={12}>
                                <ContactFormContainer />
                            </Col>
                            <Col lg={6} md={12} sm={12} xs={12}>
                                <div className="contact-strategy">
                                    <Row>
                                        <Col lg={6} md={6} sm={12} xs={12}>
                                            <div className="image-align">
                                                <img alt="" src={require("assets/img/customImages/afterSubmission.png")}/>
                                            </div>
                                            <div className="content-heading">AFTER SUBMITTING A REQUEST</div>
                                            <div className="content-sub-heading">Reply in 24 hours</div>
                                            <div className="content-para">Within 24 hours of receiving your completed form we will contact you via phone and/or email to find a convenient time to learn more about how we can help.</div>
                                        </Col> 
                                        <Col lg={6} md={6} sm={12} xs={12}>
                                            <div className="image-align">
                                                <img alt="" src={require("assets/img/customImages/project.png")}/>
                                            </div>
                                            <div className="content-heading">DEFINING PROJECT SCOPE</div>
                                            <div className="content-sub-heading">Business Need</div>
                                            <div className="content-para">We will work with you on our first call to identify your business requirements specifically and mutually decide if we are suitable for each other.</div>
                                        </Col>
                                    </Row> <br /> <br />
                                    <Row>
                                        <Col lg={6} md={6} sm={12} xs={12}>
                                            <div className="image-align">
                                                <img alt="" src={require("assets/img/customImages/approval.png")}/>
                                            </div>
                                            <div className="content-heading">AFTER APPROVAL</div>
                                            <div className="content-sub-heading">Strategy Development</div>
                                            <div className="content-para">We will then involve you more, learn more about your business, industry, competitors and challenges to build a plan tailored to your business objectives.</div>
                                        </Col>
                                        <Col lg={6} md={6} sm={12} xs={12}>
                                            <div className="image-align">
                                                <img alt="" src={require("assets/img/customImages/developmentTeam.png")}/>
                                            </div>
                                            <div className="content-heading">DEVELOPMENT TEAM</div>
                                            <div className="content-sub-heading">Application Development</div>
                                            <div className="content-para">The engineering department of Federal Soft Systems will create a strategy that makes economic sense for your company and will introduce it to you in a structured area of operation.</div>
                                        </Col>
                                    </Row>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </div>
                <div className="our-locations">
                    <span className="our">OUR</span> 
                    <span className="locations"> LOCATIONS</span>
                    <Container>
                        <div className="our-locations-images">
                            <a href="https://goo.gl/maps/xjooeavKtbF2zA628" target="_blank" rel="noopener noreferrer">
                                <div className="first-location">
                                    <div className="first-text">USA</div>
                                </div>
                            </a>
                            <a href="https://goo.gl/maps/6mfmtdKZrt6dzAEk6" target="_blank" rel="noopener noreferrer">
                                <div className="second-location">
                                    <div className="second-text">INDIA</div>
                                </div>
                            </a>
                        </div>
                    </Container>
                </div>
            </>
        )
    }
}

export default ContactPageContent;




/* <form className="contact-form" action="mailto:someone@example.com" method="post" enctype="text/plain">
    <Row>
        <Col md="6">
            <label>Name</label>
            <Input type="text" placeholder="Name" name="name" />
        </Col>
        <Col md="6">
            <label>Email</label>
            <Input type="text" placeholder="Email" name="mail" />
        </Col>
    </Row>
    <Row>
        <Col>
            <label>Message</label>
            <Input type="textarea" placeholder="Tell us your thoughts and feelings..." name="message" size="50" />
        </Col>
    </Row> <br />
    <div style={{paddingLeft: "46%"}}>
        <input type="submit" value="Send" />
    </div>
</form> */