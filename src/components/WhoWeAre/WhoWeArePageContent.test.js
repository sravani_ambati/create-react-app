import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import WhoWeArePageContent from './WhoWeArePageContent';

Enzyme.configure({ adapter: new Adapter() });

describe('WhoWeArePageContent component', () => {
  it('should show text : OUR', () => {
    const wrapper = shallow(<WhoWeArePageContent />);
    const text = wrapper.find('span.our').text();
    expect(text).toEqual('OUR');
  });
  it('should show text : LOCATIONS', () => {
    const wrapper = shallow(<WhoWeArePageContent />);
    const text = wrapper.find('span.locations').text();
    expect(text).toEqual(' LOCATIONS');
  });
});