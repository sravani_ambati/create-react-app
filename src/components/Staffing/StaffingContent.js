import React from "react";
import IndexNavbar from "components/Navbars/IndexNavbar.js";
import FooterTwo from "components/CustomFooterTwo";
import StaffingPageContent from "./StaffingPageContent";

class StaffingMainContent extends React.Component {
    render () {
        return (
            <>
                <IndexNavbar />
                <div 
                    className="page-header section-dark"
                    style={{
                        backgroundImage:
                        "url(" + require("assets/img/compressedImages/recruitment3.jpg") + ")"
                    }}
                >
                </div>
                <StaffingPageContent />
                <FooterTwo />
            </>
        )
    }
}

export default StaffingMainContent;